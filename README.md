# pass-audit

A [pass](https://www.passwordstore.org/) extension to check for compromised
passwords using Troy Hunt's [Have I Been Pwned](https://haveibeenpwned.com) Service

`pass-audit` is an extension for the Password Store to audit your passwords
against Troy Hunt's Have I Been Pwned list. It does *NOT* however leak your
passwords to the service. Your passwords are kept locally and not sent over
the network in any form. The service uses a novel way of verifying passwords
without revealing them.

The short version is:
1. Take the first 5 characters of the SHA1SUM of the password
2. Send it to the HIBP service
3. The service will return a list of all hashes that have the same 5
   characters
4. Locally verify if any of the hashes match

This method ensures that not enough bits of your password are sent to the
service to make it identifiable. Yet, its enough bits that we don't have to
download a significant chunk of the database to verify passwords. See here for
more information: https://www.troyhunt.com/ive-just-launched-pwned-passwords-version-2/

## Usage

```
Usage:

	pass audit [password|email] [data]


	pass audit password [pass-name]

		Check the password in "pass-name" against the Have I Been Pwned password
    	database. If "pass-name" is not given, then all the passwords in the
    	password store will be checked. This is the default option.


	pass audit email email-address

		Check "email-address" to see if it was included in any breaches collected
    	by Have I Been Pwned. If found, the name of the breach and the affected
    	data are printed out.

More information may be found in the pass-audit(1) man page.
```

## Installation

### From git

```
git clone https://gitlab.com/darnir/pass-audit.git
cd pass-audit
sudo make install
```

## Requirements

To use `pass-audit` you need:

  * [pass](http://www.passwordstore.org/): v1.7 or later
  * [wget](https://www.gnu.org/software/wget)


## License

[GPLv3+](https://www.gnu.org/licenses/gpl-3.0-standalone.html)

```
Copyright (C) 2018 Darshit Shah

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
