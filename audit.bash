#!/usr/bin/env bash

# pass audit - Password Store Extension (https://www.passwordstore.org/)
# Copyright (C) 2018 Darshit Shah
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# pass audit is an extension for the Password Store to audit your passwords
# against Troy Hunt's Have I Been Pwned list. It does *NOT* however leak your
# passwords to the service. Your passwords are kept locally and not sent over
# the network in any form. The service uses a novel way of verifying passwords
# without revealing them.
#
# The short version is:
# 1. Take the first 5 characters of the SHA1SUM of the password
# 2. Send it to the HIBP service
# 3. The service will return a list of all hashes that have the same 5
#    characters
# 4. Locally verify if any of the hashes match
#
# This method ensures that not enough bits of your password are sent to the
# service to make it identifiable. Yet, its enough bits that we don't have to
# download a significant chuck of the databse to verify passwords. See here for
# more information: https://www.troyhunt.com/ive-just-launched-pwned-passwords-version-2/

## Helper Functions

_send_hibp() {
	service="$1"
	param="$2"
	wget -q -O- --user-agent="pass-audit/1.0 password auditor for zx2c4 pass" --header="api-version: 2" https://api.pwnedpasswords.com/"$service"/"$param"
}

_check_password() {
	local pass_file pass_hash short_hash suffix_hash gr_out
	pass_file="$1"
	pass_hash="$($GPG -d "${GPG_OPTS[@]}" "$pass_file" |
		head -n 1 |
		tr -d '\n' |
		sha1sum |
		awk '{print $1}' |
		tr '[:lower:]' '[:upper:]'
	)"
	short_hash="$(echo "$pass_hash" | cut -c-5)"
	suffix_hash="$(echo "$pass_hash" | cut -c6-)"

	gr_out=$(_send_hibp "range" "$short_hash" |
		grep -i "$suffix_hash" |
		awk -F ":" '{print $2}' |
		tr -d '\r'
	)

	if [[ -n "$gr_out" ]]; then
		echo "$gr_out"
	else
		echo "0"
	fi
}

## Main pass audit command functions

cmd_audit_help() {
	cat <<EOF
Usage: $PROGRAM audit [password|email] [data]

  $PROGRAM audit password [pass-name]

    Check the password in "pass-name" against the Have I Been Pwned password
    database. If "pass-name" is not given, then all the passwords in the
    password store will be checked. This is the default option.


  $PROGRAM audit email email-address

    Check "email-address" to see if it was included in any breaches collected
    by Have I Been Pwned. If found, the name of the breach and the affected
    data are printed out.

For more information see the pass-audit(1) man page.
EOF
}

cmd_audit_version() {
	echo "pass audit v1.0 - Check Passwords Against HIBP Database"
	echo "Author: Darshit Shah <darnir@gmail.com>"
	echo "Source: https://www.gitlab.com/darnir/pass-audit"
}

cmd_audit_password() {
	if [[ $# -ge 1 ]]; then
		local path="${1%/}"
		local pass_file="$PREFIX/$path.gpg"
		[[ -f "$pass_file" ]] || die "Error: $1 not in password store."
		check_sneaky_paths "$path"
		times=$(_check_password "$pass_file")
		if [[ $times -eq 0 ]]; then
			echo "The password was not found in the breach database"
			return 0
		else
			echo "The password was found in the breach database $times times"
			return 2
		fi
	fi

	# The Default option is to check all passwords
	cd "$PREFIX" || die "Error: Cannot access the password store"
	shopt -s globstar
	local total_files
	declare -A breached
	local cur_count=1
	local retcode=0
	total_files=$(find . -name "*.gpg" | wc -l)
	for pass_file in **/*.gpg; do
		echo -en "\\e[0K\\rChecking passwords ... $cur_count/$total_files"
		times=$(_check_password "$pass_file")
		if [[ $times -ge 1 ]]; then
			breached["$pass_file"]=$times
		fi
		((cur_count++))
	done
	echo ""
	shopt -u globstar

	for bfile in "${!breached[@]}"; do
		tmp="${bfile#$PREFIX/}"
		echo "Password for ${tmp%.gpg} found ${breached[${bfile}]} times"
		retcode=2
	done
	return $retcode
}

cmd_audit_email() {
	# Accept an email id, and list the breaches where this email was found
	die "Not implemented yet"
}

# Don't shift on *. We send all the input to `cmd_audit_password`.
case "$1" in
	help|--help|-h) shift && cmd_audit_help ;;
	version|-v) shift && cmd_audit_version ;;
	password) shift && cmd_audit_password "$@";;
	email) shift && cmd_audit_email "$@";;
	*) cmd_audit_password "$@";;
esac
exit $?
